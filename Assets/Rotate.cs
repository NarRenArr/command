﻿using UnityEngine;

public class Rotate : MonoBehaviour, ICommand
{
    public string Description { get; set; }
    private Transform _transform;
    private float _angle;

    public Rotate(Transform t, float angle)
    {
        _transform = t;
        _angle = angle;
    }

    public void Invoke()
    {
        _transform.Rotate(Vector3.right * _angle);
    }

    public void Undo()
    {
        _transform.Rotate(-Vector3.right * _angle);
    }
}
