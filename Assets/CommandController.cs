﻿using UnityEngine;

public class CommandController : MonoBehaviour
{
    [SerializeField] private Transform _transformToManipulate;

    CommandInvoker invoker = new CommandInvoker();

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            invoker.AddCommand(new MoveForward(_transformToManipulate, 1));
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            invoker.AddCommand(new MoveForward(_transformToManipulate, -1));
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            // 90 градусов плохо отличимо в редакторе, поэтому я использую 10.
            invoker.AddCommand(new Rotate(_transformToManipulate, 10));
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            // 90 градусов плохо отличимо в редакторе, поэтому я использую 10.
            invoker.AddCommand(new Rotate(_transformToManipulate, -10));
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            invoker.ProcessAll();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            invoker.Process();
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            invoker.Undo();
        }
    }
}
