﻿public interface ICommand
{
    string Description { get; set; }

    void Invoke();
    void Undo();
}
