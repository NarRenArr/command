﻿using System.Collections.Generic;
using UnityEngine;

public class CommandInvoker
{
    private List<ICommand> _commandsQueue = new List<ICommand>();
    private List<ICommand> _commandsDone = new List<ICommand>();

    public void AddCommand(ICommand command)
    {
        _commandsQueue.Add(command);
    }

    public void ProcessAll()
    {
        foreach (ICommand c in _commandsQueue)
        {
            c.Invoke();
            _commandsDone.Add(c);
        }

        ClearQueue();
    }

    public void Process()
    {
        if (_commandsQueue.Count > 0)
        {
            _commandsQueue[0].Invoke();
            _commandsDone.Add(_commandsQueue[0]);
            _commandsQueue.RemoveAt(0);
        }
    }

    public void Undo()
    {
        if (_commandsDone.Count > 0)
        {
            _commandsDone[_commandsDone.Count - 1].Undo();
            _commandsDone.RemoveAt(_commandsDone.Count - 1);
        }
    }

    public void ClearQueue()
    {
        _commandsQueue.Clear();
        _commandsQueue.Capacity = 0;
    }
}
