﻿using UnityEngine;

public class MoveForward : MonoBehaviour, ICommand
{
    public string Description { get; set; }
    private Transform _transform;
    private float _dist;

    public MoveForward(Transform t, float dist)
    {
        _transform = t;
        _dist = dist;
    }

    public void Invoke()
    {
        _transform.position += Vector3.forward * _dist;
    }

    public void Undo()
    {
        _transform.position -= Vector3.forward * _dist;
    }
}
